import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { Router, Route, IndexRoute, browserHistory } from 'react-router';
import reduxThunk from 'redux-thunk';
import { AUTH_USER } from './actions/types';

import App from './components/app';
import Signin from './components/auth/signin';
import Signout from './components/auth/signout';
import Signup from './components/auth/signup';
import Feature from './components/feature';
import RequireAuth from './components/auth/require_auth';
import Welcome from './components/welcome';
import reducers from './reducers';
import style from '../styles/main.scss';

import UserList from './containers/user/user_list';
import BattleForm from './containers/battle/battle_form';
import BattleDetails from './containers/battle/battle_details';
import BattleList from './containers/battle/battle_list';
import BattleSong from './containers/battle/battle_details_battle_song';
import Competitors from './containers/battle/battle_details_competitors';
import MusicalPerformance from './containers/battle/battle_details_musical_performance';
import Observers from './containers/battle/battle_details_observers';
import Winner from './containers/battle/battle_details_winner';

const createStoreWithMiddleware = applyMiddleware(reduxThunk)(createStore);
const store = createStoreWithMiddleware(reducers);
const token = localStorage.getItem('token');

if(token){
  store.dispatch({type: AUTH_USER});
}

ReactDOM.render(
  <Provider store={store}>
    <Router history={browserHistory} >
      <Route path="/" component={App}>
        <IndexRoute component={Welcome} />
        <Route path="signin" component={Signin} />
        <Route path="signout" component={Signout} />
        <Route path="signup" component={Signup} />
        <Route path="feature" component={RequireAuth(Feature)} />
        <Route path="users" component={RequireAuth(UserList)} />
        <Route path="battles" component={RequireAuth(BattleList)} />
        <Route path="battle/new" component={RequireAuth(BattleForm)} />
        <Route path="battle/:battleid" component={RequireAuth(BattleDetails)}>
            <Route path="song" component={RequireAuth(BattleSong)} />
            <Route path="competitors" component={RequireAuth(Competitors)} />
            <Route path="performance" component={RequireAuth(MusicalPerformance)} />
            <Route path="observers" component={RequireAuth(Observers)} />
            <Route path="winner" component={RequireAuth(Winner)} />
        </Route>
      </Route>
    <App />
    </Router>
  </Provider>
  , document.querySelector('#react'));
