import React, { Component } from 'react';
import { reduxForm } from 'redux-form';
import * as actions from '../../actions';

class Signup extends Component {
    handleFormSubmit(formProps){
        this.props.signupUser(formProps);
    }
    renderAlert(){
        if(this.props.errorMessage){
            return (
                <div className="alert alert-danger">
                    <strong>Oops!</strong>{this.props.errorMessage}
                </div>
            ); 
        }
    }
    render(){
        const { handleSubmit, fields: {username, email, password, passwordConfirm}} = this.props;
        return (
            <div className="row">
                <div className="col-md-3 col-sm-3 col-xs-1">
                </div>
                <div className="col-md-6 col-sm-6 col-xs-10 signup-form-container">
                    <form onSubmit={handleSubmit(this.handleFormSubmit.bind(this))} autoComplete="off">
                        
                        <fieldset className={"form-group "  + "has-feedback " +
                                    ((username.touched && username.error) ? 'has-error ' : ' ') + 
                                    ((username.touched && !username.error) ? 'has-success ' : ' ')}>
                            <label>Username:</label>
                            <input className="form-control" {...username} placeholder="name"/>
                            {username.touched && username.error &&  [
                                    <span className="glyphicon glyphicon-remove form-control-feedback"></span>,
                                    <div className="error">{username.error}</div>
                            ] }
                            {username.touched && !username.error &&  [
                                    <span className="glyphicon glyphicon-ok form-control-feedback"></span>,
                            ] }
                        </fieldset>


                        <fieldset className={"form-group "  + "has-feedback " +
                                    ((email.touched && email.error) ? 'has-error ' : ' ') + 
                                    ((email.touched && !email.error) ? 'has-success ' : ' ')}>
                            <label>Email:</label>
                            <input className="form-control" {...email} placeholder="Email"/>
                            {email.touched && email.error &&  [
                                    <span className="glyphicon glyphicon-remove form-control-feedback"></span>,
                                    <div className="error">{email.error}</div>
                            ] }
                            {email.touched && !email.error &&  [
                                    <span className="glyphicon glyphicon-ok form-control-feedback"></span>,
                            ] }
                        </fieldset>
                        
                        <fieldset className={"form-group "  + "has-feedback " +
                                    ((password.touched && password.error) ? 'has-error ' : ' ') + 
                                    ((password.touched && !password.error) ? 'has-success ' : ' ')}>
                            <label>Password:</label>
                            <input type="password" className="form-control" {...password} placeholder="Password"/>
                            {password.touched && password.error &&  [
                                    <span className="glyphicon glyphicon-remove form-control-feedback"></span>,
                                    <div className="error">{password.error}</div>
                            ] }
                            {password.touched && !password.error &&  [
                                    <span className="glyphicon glyphicon-ok form-control-feedback"></span>,
                            ] }
                        </fieldset>
                        <fieldset className={"form-group "  + "has-feedback " +
                                    ((passwordConfirm.touched && passwordConfirm.error) ? 'has-error ' : ' ') + 
                                    ((passwordConfirm.touched && !passwordConfirm.error) ? 'has-success ' : ' ')}>
                            <label>Confirm Password:</label>
                            <input type="password" className="form-control" {...passwordConfirm} placeholder="Password Confirmation"/>
                            {passwordConfirm.touched && passwordConfirm.error &&  [
                                    <span className="glyphicon glyphicon-remove form-control-feedback"></span>,
                                    <div className="error">{passwordConfirm.error}</div>
                            ] }
                            {passwordConfirm.touched && !passwordConfirm.error &&  [
                                    <span className="glyphicon glyphicon-ok form-control-feedback"></span>,
                            ] }
                        </fieldset>
                        {this.renderAlert()}
                        <button action="submit" className="btn btn-primary">Sign Up</button>
                    </form>
                </div>
            </div>
            
            
        );
    }
}

function validate(formProps){

	const errors = {};

    if(!formProps.username){
		errors.username = 'Please enter a username';
	}

	if(!formProps.email){
		errors.email = "Please enter an email"
	} else {
        const emailPattern = /(.+)@(.+){2,}\.(.+){2,}/;
        if (!emailPattern.test(formProps.email)) {
            errors.email = 'Enter a valid email';
        }
    }

	if(!formProps.password){
		errors.password = 'Please enter a password';
	} else {
        const passwordPattern = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/;
        if (!passwordPattern.test(formProps.password)) {
            errors.password = 'Password must conatins minimum 8 characters at least 1 alphabet and 1 number';
        }
    }

	if(!formProps.password){
		errors.passwordConfirm = 'Please enter a password confirmation';
	}

	if(formProps.password != formProps.passwordConfirm){
        errors.passwordConfirm = "Passwords must match";
	}
    
    return errors;
}

function mapStateToProps(state){
    return { errorMessage: state.auth.error };
}


export default reduxForm({
    form: 'signup',
    fields: ['username', 'email', 'password', 'passwordConfirm'],
    validate
}, mapStateToProps, actions)(Signup);
