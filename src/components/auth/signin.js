import React, { Component } from 'react';
import { reduxForm } from 'redux-form';
import * as actions from '../../actions';

class Signin extends Component{
    handleFormSubmit({ email, password}){
        this.props.signinUser({ email, password });
    }
    renderAlert(){
        if(this.props.errorMessage){
            return(
                <div className="alert alert-danger">
                    <strong>Ooops!</strong> {this.props.errorMessage}
                </div>
            );
        }
    }
    render(){
        const { handleSubmit, fields : { email, password }} = this.props;

        return (
            <div className="row">
                <div className="col-md-3 col-sm-3 col-xs-1">
                </div>
                <div className="col-md-6 col-sm-6 col-xs-10 signup-form-container">
                    <form onSubmit={handleSubmit(this.handleFormSubmit.bind(this))} autoComplete="off">
                        <fieldset className="form-group">
                            <label>Email:</label>
                            <input {...email} className="form-control" placeholder="Email"/>
                        </fieldset> 
                        <fieldset className="form-group">
                            <label>Password:</label>
                            <input {...password} type="password" className="form-control" placeholder="Password"/>
                        </fieldset>
                        {this.renderAlert()}
                        <button action="submit" className="btn btn-primary">Sign in</button>
                    </form>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state){
    return { errorMessage: state.auth.error };
}


export default reduxForm({
    form: 'signin',
    fields : ['email', 'password']
}, mapStateToProps, actions)(Signin)