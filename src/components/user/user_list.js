import React, { Component } from 'react';

export default class UserListComponent extends Component{
    handleShowUser(){
        
    }

    handleDeleteUser(){
        console.log(this.props.user);
    }

    componentWillMount(){

    }
    render(){
        const { email, createdAt, updatedAt, id } = this.props.user;
        return(
                <tr>
                    <td>{id}</td>
                    <td>{email}</td>
                    <td>{createdAt}</td>
                    <td>{updatedAt}</td>
                    <td>
                        <button className="btn btn-primary">Show</button>
                    </td>
                    <td>
                        <button className="btn btn-danger" onClick={this.handleDeleteUser.bind(this)}>Delete</button>
                    </td>
                </tr> 
        );
    }
}