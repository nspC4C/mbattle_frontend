import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link, browserHistory } from 'react-router';
import { DropdownButton, MenuItem } from 'react-bootstrap';
class Header extends Component{

    handleRedirect(path){
        switch(path){
            case 'allBattles':
                return browserHistory.push('/battles');
            case 'newBattle':
                return browserHistory.push('/battle/new');
        }
        
    }
    renderLinks(){
        if(this.props.authenticated){
            return [
                   <li className="nav-item" key={2}>
                        <Link className="nav-link" to="/users">Users</Link>
                   </li>,
                   <DropdownButton key={5} title="Battles" id="bg-nested-dropdown" className="header-item-dropdown">
                        <MenuItem eventKey="allBattles" onSelect={this.handleRedirect}>All Batlles</MenuItem>
                        <MenuItem eventKey="newBattle" onSelect={this.handleRedirect}>Create Battle</MenuItem>
                    </DropdownButton>,
                   <li className="nav-item" key={1}>
                        <Link className="nav-link pull-rigth" to="/signout">Sign Out</Link>
                   </li>
                   ]
        } else {
            return [
                <li className="nav-item" key={1}>
                    <Link className="nav-link" to="/signin">Sign In</Link>
                </li>,
                <li className="nav-item" key={2}>
                    <Link className="nav-link" to="/signup">Sign Up</Link>
                </li>
            ]
        }
        
    }
    
    render(){
        return(
            <nav className="navbar navbar-light header">
                <Link to="/" className="navbar-brand">Music Battle</Link>
                <ul className="nav navbar-nav">
                    {this.renderLinks()}
                </ul>
            </nav>
        );
    }
}

function mapStateToProps(state){
    return{
        authenticated: state.auth.authenticated
    }
}

export default connect(mapStateToProps)(Header);