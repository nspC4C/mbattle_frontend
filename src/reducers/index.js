import { combineReducers } from 'redux';
import { reducer as form } from 'redux-form';
import authReducer from './auth_reducer';
import userReducer from './user_reducer';
import battleReducer from './battle_reducer';
const rootReducer = combineReducers({
  form,
  auth: authReducer,
  users: userReducer,
  battle: battleReducer
});

export default rootReducer;
