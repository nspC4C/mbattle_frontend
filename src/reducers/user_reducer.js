import { FETCH_USERS, DELETE_USER } from '../actions/types';

export default function(state={}, action){
    switch(action.type){
        case FETCH_USERS:
            return ( {...state, users:action.payload});
        case DELETE_USER:
            return( {...state, deletedUser:action.payload})
    }
    return state;
}