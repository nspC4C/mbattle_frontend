import { CREATE_BATTLE, FETCH_BATTLES } from '../actions/types';

export default function(state={}, action){
    switch(action.type){
        case FETCH_BATTLES:
            return{...state, battles:action.payload}
    }

    return state;
}