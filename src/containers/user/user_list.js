import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../../actions';
import UserListComponent from '../../components/user/user_list';

class UserList extends Component{ 
    onDeleteClick(user){
        this.props.deleteUser(user); 
    }

    renderUsers(){
        var self = this;
        if(this.props.users){
            return this.props.users.map(function(user, i){
                 return <tr key={user.id}>
                            <td>{user.id}</td>
                            <td>{user.email}</td>
                            <td>{user.createdAt}</td>
                            <td>{user.updatedAt}</td>
                            <td>
                                <button className="btn btn-primary">Show</button>
                            </td>
                            <td><button className="btn btn-danger" onClick={() => self.onDeleteClick(user)}>Delete</button></td>
                        </tr> 
            });
        } else {
            <div><h1>LOADING....</h1></div>
        } 
    }

    componentWillMount(){
        this.props.fetchUsers();
    }

    componentDidMount(){
        
    }

    render(){
         return (
             <div className="row user-list-row">
                <div className="col-lg-2 col-md-2 col-sm-2 col-xs-2"></div>
                <div className="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                    <table className="table table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Email</th>
                            <th>Created</th>
                            <th>Updated</th>
                            <th>Show</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                        <tbody>
                                {this.renderUsers()}
                        </tbody>
                    </table>
                </div>
            </div>
         );
    }
}

function mapStateToProps(state){
    return { 
        users: state.users.users,
        deletedUser: state.users.deletedUser 
    }
}

export default connect(mapStateToProps, actions)(UserList);
