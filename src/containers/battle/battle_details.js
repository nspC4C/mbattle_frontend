import React, { Component } from 'react';
import { Link } from 'react-router';
import { Glyphicon } from 'react-bootstrap';

export default class BattleDetails extends Component{
    render(){
        console.log(this.props)
        return(
            <div className="row">
                <div className="col-md-2">
                    
                </div>
                <div className="col-md-8 battle-details-container">
                    <div className="battle-details-header row">
                        <div className="battle-details-header-navigation-button">
                            <Link   to={`/battle/${this.props.routeParams.battleid}/song`} className="btn btn-primary navigation-button"><span>Battle Song    </span>      
                            <Glyphicon glyph="glyphicon glyphicon-music"/></Link>
                            <Link   to={`/battle/${this.props.routeParams.battleid}/competitors`} className="btn btn-primary navigation-button"><span>Competitors     </span><Glyphicon glyph="glyphicon glyphicon-fire"/></Link>
                            <Link   to={`/battle/${this.props.routeParams.battleid}/performance`} className="btn btn-primary navigation-button"><span>Musical Performance     </span><Glyphicon glyph="glyphicon glyphicon-cd"/> </Link>
                            <Link   to={`/battle/${this.props.routeParams.battleid}/observers`} className="btn btn-primary navigation-button"><span>Observers     </span><Glyphicon glyph="glyphicon glyphicon-eye-open"/> </Link>
                            <Link   to={`/battle/${this.props.routeParams.battleid}/winner`} className="btn btn-primary navigation-button"><span>Winner    </span><Glyphicon glyph="glyphicon glyphicon-tower"/> </Link>       
                        </div>
                    </div>
                    <div className="battle-details-body row">
                        {this.props.children}
                    </div>
                    <div className="battle-details-footer row">
                        FOOTER
                    </div>
                </div>
            </div>
        );
    }   
}