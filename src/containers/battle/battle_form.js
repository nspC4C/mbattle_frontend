import React, { Component } from 'react';
import { reduxForm } from 'redux-form';
import * as actions from '../../actions';

class BattleForm extends Component{

    handleFormSubmit(formProps){
        this.props.createBattle(formProps);
    }
    render(){
        const {handleSubmit, fields:{
            name,
            description, 
            preparation_time, 
            song_description,
            song_url
        }} = this.props;
        return(
            <div className="row">
                <div className="col-md-3 col-sm-3 col-xs-1"></div>
                <div className="col-md-6 col-sm-6 col-xs-10 battle_form">
                    <div className="row battle-form-header">
                        <h3>New Music Battle</h3>
                        <hr />
                    </div>
                    <form onSubmit={handleSubmit(this.handleFormSubmit.bind(this))}>
                        <fieldset className="form-group">
                            <label>Name:</label>
                            <input {...name}  className="form-control" placeholder="Battle Name" />
                            {name.touched && name.error && <div className="error">{name.error}</div> }
                        </fieldset>
                        <fieldset className="form-group">
                            <label>Description:</label>
                            <input {...description} className="form-control" placeholder="Battle Description" />
                            {description.touched && description.error && <div className="error">{description.error}</div> }
                        </fieldset>
                        <fieldset className="form-group">
                            <label>Preparation Time Due To:</label>
                            <input {...preparation_time} className="form-control" placeholder="Preparation Time" />
                            {preparation_time.touched && preparation_time.error && <div className="error">{preparation_time.error}</div> }
                        </fieldset>
                        <fieldset className="form-group">
                            <label>Youtube Song URL:</label>
                            <input {...song_url} className="form-control" placeholder="Youtube URL" />
                            {song_url.touched && song_url.error && <div className="error">{song_url.error}</div> }
                        </fieldset>
                        <button action="submit" className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        );
    }
}

function validate(formProps){

	const errors = {};
    
	if(!formProps.name){
		errors.name = "Please enter a name"
	}

    if(!formProps.description){
		errors.description = "Please enter a description"
	}

    if(!formProps.preparation_time){
		errors.preparation_time = "Please enter a preparation time"
	}

    if(!formProps.song_url){
		errors.song_url = "Please enter a song url"
	}
    
    return errors;
}





export default reduxForm({
    form: 'signin',
    fields : [
        'name', 
        'description', 
        'preparation_time', 
        'song_description', 
        'song_url',
        'song_embedded_url' ],
    validate
},null, actions)(BattleForm)