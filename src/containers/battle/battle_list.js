import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../../actions';
import { Link } from 'react-router';
import Gravatar from 'react-gravatar';
import { Button, Glyphicon } from 'react-bootstrap';
import _ from 'lodash';

class BattleList extends Component{
    componentWillMount(){
        this.props.fetchBattles();
    }
    renderList(){
        if(this.props.battles){
            var d = new Date();
            
            return this.props.battles.map(function(battle){
                return <div className="row" key={battle.id}>
                                <div className="col-md-3 col-sm-3 col-xs-hidden">
                                </div>
                                <div className="col-md-6 col-sm-6 battle-list-item">
                                    <div className="row battle-list-item-header">
                                        <div className="col-md-1 col-sm-1 col-xs-1 battle-list-item-header-avatar">
                                            <Gravatar email={battle.User.email} className="avatar-image-list" https default="mm" rating="g"/>
                                        </div>
                                        <div className="col-md-8 col-sm-8 col-xs-8 battle-list-item-header-text">
                                            <div className="row">
                                                <span className="battle-list-item-username">{battle.User.username}</span>
                                                <div className="divider" />
                                                <span>created battle on guitar:</span>
                                                <div className="divider" />
                                                <Link to={`battle/${battle.id}/song`}>{battle.name}.</Link>
                                            </div>
                                            <div className="row">
                                               created: {battle.createdAt}
                                            </div>
                                            
                                        </div>
                                    </div>  
                                        <hr />
                                    <div className="row battle-list-item-body">
                                        <div className="col-lg-8 col-md-12 col-sm-12">
                                            <iframe width="100%"
                                                    height="315"
                                                    class-name="battle-list-item-video"
                                                    src={`https://${battle.BattleSong.embedded_url}`}>
                                            </iframe>
                                        </div>
                                        <div className="col-md-4 col-lg-4 hidden-sm hidden-md hidden-xs battle-list-item-competitors">
                                            <h5 className="battle-list-item-competitor-list-header">Competitors</h5>
                                            <hr />
                                            <div className="battle-list-item-competitor-list">
                                                <div className="battle-list-item-competitor-list-item">
                                                <Gravatar   email={battle.User.email} 
                                                            className="avatar-image-battle-list-item-competitor-list" 
                                                            size={50} 
                                                            https
                                                            default="mm" 
                                                            rating="g"/>
                                                    <span className="battle-list-item-competitor-list-item-username">Adam Ogórek</span>
                                                </div>
                                                <div className="battle-list-item-competitor-list-item">
                                                <Gravatar email={battle.User.email} className="avatar-image-battle-list-item-competitor-list" size={50} default="mm" rating="g"/>
                                                    Anna Dynia
                                                </div>
                                                <div className="battle-list-item-competitor-list-item">
                                                <Gravatar email={battle.User.email} className="avatar-image-battle-list-item-competitor-list" size={50} default="mm" rating="g"/>
                                                    Jan Pomidor
                                                </div>
                                                <div className="battle-list-item-competitor-list-item">
                                                <Gravatar email={battle.User.email} className="avatar-image-battle-list-item-competitor-list" size={50} default="mm" rating="g"/>
                                                    Jerzy Kowalski
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row battle-list-item-footer">
                                        <div className="battle-list-item-footer-content">
                                            <Button className="battle-list-item-footer-content-button">
                                                <Glyphicon glyph="glyphicon glyphicon-glass"/> 561 <span hidden>Drinks</span></Button>
                                            <Button className="battle-list-item-footer-content-button">
                                                <Glyphicon glyph="glyphicon glyphicon-eye-open"/> 1000 <span hidden>Views</span></Button>
                                            <Button className="battle-list-item-footer-content-button">
                                                <Glyphicon glyph="glyphicon glyphicon-user"/> 4 <span hidden>Competitors</span></Button>
                                            <Button className="battle-list-item-footer-content-button">
                                                <Glyphicon glyph="glyphicon glyphicon-music"/>15433<span hidden>Points To Win</span></Button>
                                        </div>
                                    </div>
                                </div>
                        </div>
                        // hidden-md-up col-sm-3
            })
        }
    }
    render(){
        return(
            <div>
                {this.renderList()}
            </div>
        );
    }
}

function mapStateToProps (state){
    return  {battles: state.battle.battles};
}

export default connect(mapStateToProps, actions)(BattleList);