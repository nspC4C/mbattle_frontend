import React, { Component } from 'react';
import Gravatar from 'react-gravatar';

export default class BattleDetailsBattleSong extends Component{
     render(){
         return(
             <ul className="list-group">
                <li className="list-group-item competitor">
                    <div className="row">
                        <div className="col-md-2">
                            <Gravatar   email="daniiel.zielinski@gmail.com" className="avatar-image-battle-detail-competitor-list" size={100} https default="mm"  rating="g"/>
                        </div>
                        <div className="col-md-4">
                            <div className="row">
                                Username: Daniel
                            </div>
                            <div className="row">
                                Battles: 5465
                            </div>
                            <div className="row">
                                Wins: 4000
                            </div>
                            <div className="row">
                                Loses: 1465
                            </div>      
                        </div>
                        <div className="col-md-6">
                            Points: 87234
                        </div>
                        
                    </div>    
                </li>
                <li className="list-group-item competitor">
                    <div className="row">
                        <div className="col-md-2">
                            <Gravatar   email="daniiel.zielinski@gmail.com" className="avatar-image-battle-detail-competitor-list" size={100} https default="mm"  rating="g"/>
                        </div>
                        <div className="col-md-4">

                        </div>
                        <div className="col-md-6">

                        </div>
                        
                    </div>   
                </li>
             </ul>
         );
     }
}