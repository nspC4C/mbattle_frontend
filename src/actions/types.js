export const AUTH_USER = 'auth_user';
export const UNAUTH_USER = 'unauth_user';
export const AUTH_ERROR = "auth_errors";
export const FETCH_MESSAGE = "fetch_message";

export const FETCH_USERS = "fetch_users";
export const DELETE_USER = "delete_user";

export const CREATE_BATTLE = "create_battle";
export const FETCH_BATTLES = "fetch_battles";
