import axios from 'axios';
import { browserHistory } from 'react-router';
import {   
    AUTH_USER, 
    AUTH_ERROR, 
    UNAUTH_USER, 
    FETCH_MESSAGE,
    FETCH_USERS,
    DELETE_USER,
    CREATE_BATTLE,
    FETCH_BATTLES } from './types';
 
var ROOT_URL;
var API_ROOT_URL;
if(window.location.href.includes("localhost")){
    ROOT_URL = 'http://localhost:3090';
	API_ROOT_URL = 'http://localhost:3090/api/';
} else if (window.location.href.includes("herokuapp")){
    ROOT_URL = 'https://music-battle-api.herokuapp.com';
	API_ROOT_URL = 'https://music-battle-api.herokuapp.com/api/';
}



export function signinUser({ email, password}){
    return function(dispatch){
        axios.post(`${ROOT_URL}/signin`, {email, password})
            .then(response => {
                //set state to authenticated=true
                dispatch({ type: AUTH_USER });
                //save token to local storage
                localStorage.setItem('token', response.data.token);
                // redirect to protected route
                browserHistory.push('/battles');
            })
            .catch(() => {
                dispatch(authError('Bad Login Info'));
            });
    }
}

export function signupUser({username, email, password}){
    return function(dispatch){
        axios.post(`${ROOT_URL}/signup`, {username, email, password})
            .then(response => {
                    dispatch({ type: AUTH_USER });
                    localStorage.setItem('token', response.data.token);
                    browserHistory.push('/battles');
            })
            .catch(response => dispatch(authError(response.error)));
                
    }
}

export function authError(error){
    return {
        type: AUTH_ERROR,
        payload: error
    }
}

export function signoutUser(){
    localStorage.removeItem('token');
    return { type: UNAUTH_USER };
}

export function fetchMessage(){
    return function(dispatch){
        axios.get(ROOT_URL, {
            headers: { authorization: localStorage.getItem('token') }
        })
            .then(response =>{
                dispatch({
                    type: FETCH_MESSAGE,
                    payload: response.data.message
                })
            })
    }
}

export function fetchUsers(){
    return function(dispatch){
        axios.get(`${API_ROOT_URL}users`, {
            headers : { authorization: localStorage.getItem('token') }
        })
            .then(response => {
                dispatch({
                    type: FETCH_USERS,
                    payload: response.data
                })
            })
    }
}


export function deleteUser(user){
    return function(dispatch){
        axios.delete(`${API_ROOT_URL}user/${user.id}`, {
            headers : { authorization: localStorage.getItem('token') }
        })
            .then(response => {
                dispatch({
                    type: DELETE_USER,
                    payload: user
                })
            })
    }
}


export function createBattle(battle){
    return function(dispatch){
        axios.post(`${API_ROOT_URL}battle/new`, battle ,{
            headers : { authorization: localStorage.getItem('token') }
        })
            .then(response => {
                    dispatch({ type: CREATE_BATTLE });
                    browserHistory.push('/battle/'+response.data.battle.id);
            })
    }
}

export function fetchBattles(){
    return function(dispatch){
        axios.get(`${API_ROOT_URL}battles`, {
            headers : { authorization: localStorage.getItem('token') }
        })
            .then(response => {
                dispatch({
                    type: FETCH_BATTLES,
                    payload: response.data.battles
                })
            })
    }
}